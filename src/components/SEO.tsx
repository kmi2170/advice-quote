import Head from 'next/head';

const SEO = () => {
  const title = config.title;
  const siteTitle = config.title;
  const author = config.author;
  const description = config.description;
  const keywords = config.keywords;

  return (
    <Head>
      <title>{`${siteTitle}`}</title>
      <meta name="author" content={author} />
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
      <meta property="og:type" content="website" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content={siteTitle} />
      <meta property="twitter:card" content="summary" />
      <meta property="twitter:creator" content={config.social.twitter} />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={description} />
    </Head>
  );
};

export default SEO;

const config = {
  title: 'Advice/Quotes App',
  author: 'Kmi, Web developer/programmer',
  // author: {
  //   name: 'Kmi',
  //   summary: 'Web developer/programmer',
  // },
  description:
    'A simple web application that displays randomly chosen Advice or Quote, created with Next.js, Typescript adn Material-UI. It is switchable between Advice and Quote. Quote can be narrowed down by selecting a category. A little addition is that you can choose between two background images, bamboos and flowers. Your selections of Advice/Quote and background image are stored in cookie if available and peresisted when you come back to this app next time',
  keywords: 'Next.js, TypeScript, Material-UI, Advice, Quote',
  social: {
    twitter: '',
  },
};
